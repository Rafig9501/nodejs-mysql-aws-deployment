resource "aws_appautoscaling_target" "ecs_target" {
  max_capacity       = var.auto_scale_max_capacity
  min_capacity       = var.auto_scale_min_capacity
  resource_id        = "service/${aws_ecs_cluster.nodejs-cluster.name}/${aws_ecs_service.nodejs-service.name}"
  scalable_dimension = var.auto_scale_target_dimension
  service_namespace  = var.auto_scale_target_namespace
}

resource "aws_appautoscaling_policy" "ecs_policy_up" {
  name               = var.auto_scale_policy_name
  policy_type        = var.auto_scale_policy_type
  resource_id        = "service/${aws_ecs_cluster.nodejs-cluster.name}/${aws_ecs_service.nodejs-service.name}"
  scalable_dimension = aws_appautoscaling_target.ecs_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_target.service_namespace

  step_scaling_policy_configuration {
    adjustment_type         = var.auto_scale_adjustment_type
    cooldown                = var.auto_scale_policy_cooldown
    metric_aggregation_type = var.metric_aggregation_type

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = -1
    }
  }
}
