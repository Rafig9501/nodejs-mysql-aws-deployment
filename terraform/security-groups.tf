resource "aws_security_group" "nodejs-alb-sg" {
  name        = var.sg_alb_name
  vpc_id      = aws_vpc.nodejs-vpc.id

  ingress {
    protocol    = "tcp"
    from_port   = var.alb_listener_port
    to_port     = var.alb_listener_port
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "NodeJS ALB Security Group"
  }
}

resource "aws_security_group" "ecs_sg" {
  name        = var.sg_ecs_name
  vpc_id      = aws_vpc.nodejs-vpc.id

  ingress {
    protocol        = "tcp"
    from_port       = var.app_port
    to_port         = var.app_port
    security_groups = [aws_security_group.nodejs-alb-sg.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "ECS Security Group"
  }
}

resource "aws_security_group" "mysql_sg" {
  name = var.sg_mysql_name
  vpc_id = aws_vpc.nodejs-vpc.id

  ingress {
    from_port = var.db_port
    protocol  = "tcp"
    to_port   = var.db_port
    security_groups = [aws_security_group.ecs_sg.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "MySQL Security Group"
  }
}