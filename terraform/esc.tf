resource "aws_ecs_cluster" "nodejs-cluster" {
  name       = var.cluster_name
  tags       = {
    Name = "ECS Cluster"
  }
  depends_on = [aws_db_instance.nodejsmysqldb]
}

resource "aws_ecs_task_definition" "nodejs-def" {
  family                   = var.ecs_task_family
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  container_definitions    = jsonencode([
    {
      logConfiguration : {
        "logDriver" : "awslogs",
        "options" : {
          "awslogs-group" : "/ecs/nodejs_taskd",
          "awslogs-region" : var.aws_region,
          "awslogs-stream-prefix" : "ecs"
        }
      }
      cpu : var.container_definition_cpu,
      memory : var.fargate_memory,
      portMappings : [
        {
          "hostPort" : var.app_port,
          "protocol" : "tcp",
          "containerPort" : var.app_port
        }
      ],
      environment : [
        {
          "name" : "DB_HOST",
          "value" : aws_db_instance.nodejsmysqldb.address
        },
        {
          "name" : "DB_USER",
          "value" : var.MYSQL_USERNAME
        },
        {
          "name" : "DB_PASSWORD",
          "value" : var.MYSQL_PASSWORD
        }
      ],
      image : var.app_image,
      essential : true,
      name : var.container_name
    }
  ])
  tags                     = {
    Name = "ECS Cluster Task Definition"
  }
}

data "aws_ecs_container_definition" "nodejs-app" {
  container_name  = var.container_name
  task_definition = aws_ecs_task_definition.nodejs-def.id
}

resource "aws_ecs_service" "nodejs-service" {
  name            = var.ecs_service_name
  cluster         = aws_ecs_cluster.nodejs-cluster.id
  task_definition = aws_ecs_task_definition.nodejs-def.arn
  desired_count   = var.app_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.ecs_sg.id]
    subnets          = aws_subnet.private_ecs_subnet.*.id
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.nodejs-alb-tg.arn
    container_name   = var.container_name
    container_port   = var.app_port
  }

  depends_on = [aws_alb_listener.nodejs-app, aws_iam_role_policy_attachment.ecs_task_execution_role]

  tags = {
    Name = "ECS Cluster Service"
  }
}