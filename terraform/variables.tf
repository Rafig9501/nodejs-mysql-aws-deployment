variable "az_count" {
  default     = "2"
  description = "number of availability zones in above region"
}

variable "vpc_ip" {
  default = "10.0.0.0/16"
  description = "ip address of vpc"
}

variable "ecs_task_family" {
  default = "nodejs-app-task"
}

variable "ecs_service_name" {
  default = "nodejs-app-service"
}

variable "rds-type" {
  default = "db.t2.medium"
}

variable "allocated_storage_rds" {
  default = 20
}

variable "engine_rds" {
  default = "mysql"
}

variable "engine_version_rds" {
  default = "8.0"
}

variable "MYSQL_USERNAME" {
  default = ""
}

variable "MYSQL_PASSWORD" {
  default = ""
}

variable "rds_private_subnet_ip" {
  default = "10.0.5.0/24"
}

variable "rds_public_subnet_ip" {
  default = "10.0.6.0/24"
}

variable "storage_type_rds" {
  default = "gp2"
}

variable "max_allocate_storage_rds" {
  default = 30
}

variable "db_param_family" {
  default = "mysql8.0"
}

variable "db_param_group_name" {
  default = "mysql-param-group"
}

variable "db_subnet_group_name" {
  default = "mysql-subnet-group"
}

variable "sg_alb_name" {
  default = "nodejs-load-balancer-security-group"
}

variable "sg_ecs_name" {
  default = "nodejs-ecs-tasks-security-group"
}

variable "db_identifier_rds" {
  default = "nodejs-mysql"
}

variable "db_name" {
  default = "nodejsmysqldb"
}

variable "aws_region" {
  default = "us-east-1"
  description = "AWS Region"
}

variable "ecs_task_execution_role" {
  default     = "ECSTaskExecutionRole"
  description = "ECS task execution role name"
}

variable "app_image" {
  default     = "registry.gitlab.com/rafig9501/nodejs-mysql-aws-deployment:latest"
  description = "docker image to run in this ECS cluster"
}

variable "cluster_name" {
  default = "ecs-cluster"
}

variable "alb_listener_port" {
  default = 80
}

variable "app_port" {
  default     = 3000
  description = "port exposed on the docker image"
}

variable "db_port" {
  default = 3306
  description = "port for mysql database"
}

variable "app_count" {
  default     = "2"
  description = "number of docker containers to run"
}

variable "container_name" {
  default = "nodejs-app"
}

variable "sg_mysql_name" {
  default = "mysql_security_group"
}

variable "az_1" {
  default = "us-east-1a"
}

variable "az_2" {
  default = "us-east-1b"
}

variable "health_check_path" {
  default = "/"
}

variable "auto_scale_policy_name" {
  default = "auto_scale_policy"
}

variable "alb_target_protocol" {
  default = "HTTP"
}

variable "alb_target_type" {
  default = "ip"
}

variable "alb_name" {
  default = "nodejs-load-balancer"
}

variable "alb_target_name" {
  default = "nodejs-target-group"
}

variable "auto_scale_max_capacity" {
  default = 4
}

variable "auto_scale_policy_cooldown" {
  default = 60
}

variable "metric_aggregation_type" {
  default = "Average"
}

variable "auto_scale_min_capacity" {
  default = 2
}

variable "auto_scale_policy_type" {
  default = "StepScaling"
}

variable "auto_scale_target_dimension" {
  default = "ecs:service:DesiredCount"
}

variable "auto_scale_target_namespace" {
  default = "ecs"
}

variable "auto_scale_adjustment_type" {
  default = "ChangeInCapacity"
}

variable "fargate_cpu" {
  default     = "1024"
  description = "fargate instance CPU units to provision,my requirement 1 cpu so gave 1024"
}

variable "container_definition_cpu" {
  default = 1
}

variable "fargate_memory" {
  default     = 2048
  description = "Fargate instance memory to provision in MiB"
}
