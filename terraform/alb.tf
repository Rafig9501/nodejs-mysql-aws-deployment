resource "aws_alb" "nodejs-alb" {
  name           = var.alb_name
  subnets        = aws_subnet.public_ecs_subnet.*.id
  security_groups = [aws_security_group.nodejs-alb-sg.id]

  tags = {
    Name = "NodeJS Load Balancer"
  }
}

resource "aws_alb_target_group" "nodejs-alb-tg" {
  name        = var.alb_target_name
  port        = var.app_port
  protocol    = var.alb_target_protocol
  target_type = var.alb_target_type
  vpc_id      = aws_vpc.nodejs-vpc.id

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    protocol            = var.alb_target_protocol
    matcher             = "200"
    path                = var.health_check_path
    interval            = 30
  }

  tags = {
    Name = "NodeJS Load Balancer Target Group"
  }
}

resource "aws_alb_listener" "nodejs-app" {
  load_balancer_arn = aws_alb.nodejs-alb.id
  port              = var.alb_listener_port
  protocol          = var.alb_target_protocol
  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.nodejs-alb-tg.arn
  }

  tags = {
    Name = "NodeJS Load Balancer Listener"
  }
}