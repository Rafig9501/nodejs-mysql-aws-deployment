resource "aws_db_subnet_group" "rds_subnet_group" {
  name       = var.db_subnet_group_name
  tags = {
    "Name" = "Mysql Subnet Group"
  }
  subnet_ids = [aws_subnet.private_rds_subnet.id, aws_subnet.public_rds_subnet.id]
}

resource "aws_db_parameter_group" "mysql_param_group" {
  name   = var.db_param_group_name
  family = var.db_param_family

  parameter {
    name  = "character_set_server"
    value = "utf8"
  }

  parameter {
    name  = "character_set_client"
    value = "utf8"
  }
}

resource "aws_db_instance" "nodejsmysqldb" {
  allocated_storage = var.allocated_storage_rds
  engine = var.engine_rds
  engine_version = var.engine_version_rds
  instance_class = var.rds-type
  name = var.db_name
  username = var.MYSQL_USERNAME
  password = var.MYSQL_PASSWORD
  storage_type = var.storage_type_rds
  publicly_accessible = false
  multi_az = false
  max_allocated_storage = var.max_allocate_storage_rds
  skip_final_snapshot = true
  identifier = var.db_identifier_rds
  db_subnet_group_name = aws_db_subnet_group.rds_subnet_group.name
  vpc_security_group_ids = [aws_security_group.mysql_sg.id]
}
