resource "aws_vpc" "nodejs-vpc" {
  cidr_block = var.vpc_ip
  tags = {
    Name = "ECS VPC"
  }
}

data "aws_availability_zones" "available_az" {
}

resource "aws_subnet" "private_ecs_subnet" {
  count             = var.az_count
  cidr_block        = cidrsubnet(aws_vpc.nodejs-vpc.cidr_block, 8, count.index)
  availability_zone = data.aws_availability_zones.available_az.names[count.index]
  vpc_id            = aws_vpc.nodejs-vpc.id
  tags = {
    Name = format("Private Subnet %d", count.index)
  }
}

resource "aws_subnet" "public_ecs_subnet" {
  count                   = var.az_count
  cidr_block              = cidrsubnet(aws_vpc.nodejs-vpc.cidr_block, 8, var.az_count + count.index)
  availability_zone       = data.aws_availability_zones.available_az.names[count.index]
  vpc_id                  = aws_vpc.nodejs-vpc.id
  map_public_ip_on_launch = true
  tags = {
    Name = format("Public Subnet %d", count.index)
  }
}

resource "aws_subnet" "private_rds_subnet" {
  availability_zone = var.az_1
  cidr_block = var.rds_private_subnet_ip
  vpc_id     = aws_vpc.nodejs-vpc.id
}

resource "aws_subnet" "public_rds_subnet" {
  availability_zone = var.az_2
  cidr_block = var.rds_public_subnet_ip
  vpc_id     = aws_vpc.nodejs-vpc.id
}

resource "aws_internet_gateway" "test-igw" {
  vpc_id = aws_vpc.nodejs-vpc.id
  tags = {
    Name = "Internet Gateway"
  }
}

resource "aws_route" "internet_access" {
  route_table_id         = aws_vpc.nodejs-vpc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.test-igw.id
}

resource "aws_eip" "nat-gt-eip" {
  count      = var.az_count
  vpc        = true
  depends_on = [aws_internet_gateway.test-igw]
  tags = {
    Name = "Nate Gateway EIP"
  }
}

resource "aws_nat_gateway" "test-nat-gw" {
  count         = var.az_count
  subnet_id     = element(aws_subnet.public_ecs_subnet.*.id, count.index)
  allocation_id = element(aws_eip.nat-gt-eip.*.id, count.index)
  tags = {
    Name = "ECS Nate Gateway"
  }
}

resource "aws_route_table" "private-rt" {
  count  = var.az_count
  vpc_id = aws_vpc.nodejs-vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = element(aws_nat_gateway.test-nat-gw.*.id, count.index)
  }
  tags = {
    Name = "Private Route Table"
  }
}

resource "aws_route_table_association" "private_rt_association" {
  count          = var.az_count
  subnet_id      = element(aws_subnet.private_ecs_subnet.*.id, count.index)
  route_table_id = element(aws_route_table.private-rt.*.id, count.index)
}
