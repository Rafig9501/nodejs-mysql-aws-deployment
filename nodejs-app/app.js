let express = require('express');
let path = require('path');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');

let routes = require('./routes/index');
let users = require('./routes/users');

// DataBase
let mysql = require("mysql2");

let db_host = process.env.DB_HOST
let db_user = process.env.DB_USER
let db_password = process.env.DB_PASSWORD

let con = mysql.createConnection({
    host: db_host,
    user: db_user,
    password: db_password
});

con.connect(function (err) {
    if (err) throw err;
    con.query("CREATE DATABASE IF NOT EXISTS USERS_DB", function (err, result) {
        if (err) throw err;
        console.log("Database created");
    });
    con.query(
        "USE USERS_DB;", function (err, result) {
            if (err) throw err;
            console.log(result);
        });
    con.query("CREATE TABLE IF NOT EXISTS USER_ACCOUNTS(id int(11) NOT NULL, userid varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL, password varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL, email varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL, PRIMARY KEY (id), UNIQUE KEY id (id), KEY id_2 (id), KEY id_3 (id)) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci;\n"
        , function (err, result) {
            if (err) throw err;
            console.log(result);
        });
    con.query("REPLACE INTO USER_ACCOUNTS (id, userid, password, email) VALUES (1, 'admin', '1234', 'sylwia@gmail.com'), (2, 'user', 'user', 'ann@gmail.com'), (3, 'jason', '1234', 'jason@json123.com'), (4, 'kuku', '1111', 'elay@gmail.com');", function (err, result) {
        if (err) throw err;
        console.log(result);
    });
    con.query("ALTER TABLE USER_ACCOUNTS MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 10;", function (err, result) {
        if (err) throw err;
        console.log(result);
    });

});

con.connect(function (err) {
    if (err) {
        console.log('connecting error', err);
        return;
    }
    console.log('connecting success');
});

let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// db state
app.use(function (req, res, next) {
    req.con = con;
    next();
});

app.use('/', routes);
app.use('/users', users);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
