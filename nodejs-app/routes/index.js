let express = require('express');
let router = express.Router();

// home page
router.get('/', function(req, res, next) {

    let db = req.con;
    let data = "";

    let user = req.query.user;

    let filter = "";
    if (user) {
        filter = 'WHERE userid = ?';
    }

    db.query('SELECT * FROM USER_ACCOUNTS ' + filter, user, function(err, rows) {
        if (err) {
            console.log(err);
        }
        let data = rows;

        // use index.ejs
        res.render('index', { title: 'USER_ACCOUNTS Information', data: data, user: user });
    });

});

// add page
router.get('/add', function(req, res, next) {

    // use userAdd.ejs
    res.render('userAdd', { title: 'Add User', msg: '' });
});

// add post
router.post('/userAdd', function(req, res, next) {

    let db = req.con;

    // check userid exist
    let userid = req.body.userid;
    let qur = db.query('SELECT userid FROM USER_ACCOUNTS WHERE userid = ?', userid, function(err, rows) {
        if (err) {
            console.log(err);
        }

        let count = rows.length;
        if (count > 0) {

            let msg = 'Userid already exists.';
            res.render('userAdd', { title: 'Add User', msg: msg });

        } else {

            let sql = {
                userid: req.body.userid,
                password: req.body.password,
                email: req.body.email
            };

            //console.log(sql);
            let qur = db.query('INSERT INTO USER_ACCOUNTS SET ?', sql, function(err, rows) {
                if (err) {
                    console.log(err);
                }
                res.setHeader('Content-Type', 'application/json');
                res.redirect('/');
            });
        }
    });


});

// edit page
router.get('/userEdit', function(req, res, next) {

    let id = req.query.id;
    //console.log(id);

    let db = req.con;
    let data = "";

    db.query('SELECT * FROM USER_ACCOUNTS WHERE id = ?', id, function(err, rows) {
        if (err) {
            console.log(err);
        }

        let data = rows;
        res.render('userEdit', { title: 'Edit USER_ACCOUNTS', data: data });
    });

});


router.post('/userEdit', function(req, res, next) {

    let db = req.con;

    let id = req.body.id;

    let sql = {
        userid: req.body.userid,
        password: req.body.password,
        email: req.body.email
    };

    let qur = db.query('UPDATE USER_ACCOUNTS SET ? WHERE id = ?', [sql, id], function(err, rows) {
        if (err) {
            console.log(err);
        }

        res.setHeader('Content-Type', 'application/json');
        res.redirect('/');
    });

});


router.get('/userDelete', function(req, res, next) {

    let id = req.query.id;

    let db = req.con;

    let qur = db.query('DELETE FROM USER_ACCOUNTS WHERE id = ?', id, function(err, rows) {
        if (err) {
            console.log(err);
        }
        res.redirect('/');
    });
});

module.exports = router;
